# DRF Advanced

Build a Backend REST API with Python & Django - Advanced

## Udemy Course Page

[https://www.udemy.com/course/django-python-advanced/](https://www.udemy.com/course/django-python-advanced/)

## Course Code Checker
[https://codechecker.app/checker/londonappdev/start/recipe-app-api-2/](https://codechecker.app/checker/londonappdev/start/recipe-app-api-2/)

## Project Folder
```bash
~/udemy/drf-advanced/recipe-app-api
```